import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner myscan = new Scanner (System.in);
        System.out.println("Enter the word");
        String w = myscan.nextLine();
        try {
            String word = Length.getLength(w);
            System.out.println("The word is correct");
        } catch (LengthException ex) {

            System.out.println(ex.getMessage());

        }
    }
}
class Length{

    public static String getLength(String word) throws LengthException{

        if(word.length()>10) throw new LengthException("The word has more than 10 letters");

        return word;
    }
}

class LengthException extends Exception{
    public LengthException(String message){

        super(message);
    }
}